import React, { Component } from 'react';
import './App.sass';
import '../node_modules/bootstrap/scss/bootstrap.scss';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Home from './components/Home/Home';
import About from './components/About/About';
import Contact from './components/Contact/Contact';
import Sass from './components/Tutorials/Sass/Sass';
import Html5 from './components/Tutorials/Html5/Html5';
import JavaScript from './components/Tutorials/JavaScript/JavaScript';
import RReact from './components/Tutorials/React/React';
import Git from './components/Tutorials/Git/Git';
import Blog from './components/Blog/Blog';
import { Route } from 'react-router-dom';






class App extends Component {
  render() {
    return (
      <div className = 'container-fluid content'>
        <Header />
          <Route exact={true} path="/" component={Home} />
          <Route path="/About" component={About} />
          <Route path="/Blog" component={Blog} />
          <Route path="/Contact" component={Contact} />
          <Route path="/Tutorials/Sass" component={Sass} />
          <Route path="/Tutorials/Html5" component={Html5} />
          <Route path="/Tutorials/JavaScript" component={JavaScript} />
          <Route path="/Tutorials/React" component={RReact} />
          <Route path="/Tutorials/Git" component={Git} />

        <Footer />
      </div>
    );
  }
}
export default App;
