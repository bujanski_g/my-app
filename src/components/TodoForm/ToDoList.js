import React from 'react';
import './ToDoList.sass';

const ToDoList = props => (
  <div className='Todo'>
    <ul>
      {
        props.value.map((item, index) => <li key={index}>{item}</li>)
      }
    </ul>
  </div>
);

export default ToDoList;
