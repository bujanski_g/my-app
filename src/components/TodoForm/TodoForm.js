import React, { Component } from 'react';
import './TodoForm.sass';
import ToDoList from './ToDoList';



class TodoForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
                  tmpValue: '',
                  value: []
                  };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = (event) => {
    this.setState({tmpValue: event.target.value});

  }

  handleSubmit = (event) => {
    document.getElementById("form").reset();
    event.preventDefault();
    if (this.state.tmpValue == ''){
      alert('pustego nie dodasz :(');}
    else if (this.state.tmpValue == ' '){
      alert('serio? spacja?');}
    else {
      this.setState({
        tmpValue: '',
        value: [...this.state.value, this.state.tmpValue]
      });
    }

  }

render() {
  return (
    <div>
      <div>
        <form id='form' onSubmit={this.handleSubmit}>
          <h4>ToDo</h4>
            <div className="input-group mb-3">
              <input type="text" className="form-control" aria-describedby="button-addon2" onChange={this.handleChange} />
                <div className="input-group-append">
                  <button className="btn btn-outline-secondary" type="submit">Dodaj</button>
                </div>
            </div>
        </form>
      </div>
      <div>
         <ToDoList value={this.state.value} />
      </div>
    </div>
  );
}
}
export default TodoForm;
