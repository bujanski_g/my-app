import React, { Component } from 'react';
import './Blog.sass';


class Blog extends Component {
  render() {
    return (

      <div className = "content">
        <h1>Moje spostrzeżenia i przemyślenia podczas pisania projektu</h1>
          <div>
            <ul>
              <li>Jestem skazany na bootstrapa, ale to dobrze?</li>
              <li>Przyśpiesza pisanie projektu</li>
              <li>Ostatecznie czy to Bootstrap, czy Bulma, czy cokolwiek innego zawsze będzie coś, co mi nie pasuje</li>
              <li>Bootstrap jest trochę must have jeśli chodzi o jego znajomość</li>
              <li>CSS w React jest specyficzny. To co się powtarza piszesz raz. Potem się dziedziczy w magiczny sposób...</li>
              <li>Bardzo szybko zaczne pisać automat do dodawania postów. Dodawanie tego w kodzie jest mega męczące</li>
              <li>Opa ma jednak swoje wady. Jest szybkie, ale jednak przydałoby się coś innego - prędzej czy później sięgne po inne rozwiązanie do bloga</li>
            </ul>
          </div>
      </div>

    );
  }
}
export default Blog;
