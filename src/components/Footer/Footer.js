import React, { Component } from 'react';
import './Footer.sass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelopeOpen } from '@fortawesome/free-regular-svg-icons';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';

class Footer extends Component {
  render() {
    return (
      <div className = "container-fluid footer-content">
        <div className="footer">
          <div className = "row">
            <div className= "col left">grz3ch</div>
            <div className= "col-5 middle">Nie kopiować</div>
            <div className= "col right">
              <span><FontAwesomeIcon icon={faFacebook} /></span>
              <span><FontAwesomeIcon icon={faTwitter} /></span>
              <span><FontAwesomeIcon icon={faEnvelopeOpen} /></span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Footer;
