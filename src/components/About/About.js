import React, { Component } from 'react';
import "./About.sass";


class About extends Component {
  render() {
    return (

      <div className = "About-container">
        <div className = "About-text">
          <p>Strona prowadzona w celach badawczych. Na potrzeby psucia, naprawiania, wkuriwania się i szeroko pojętego rozwijania własnych kompetencji jako Web developer.</p>
          <p>Przed użyciem zapoznaj sie z ulotką bądź skontaktuj się ze swym lekarzem psychiatrą.</p>
        </div>
        <div className = "About-text">
          <p>Jeśli jakimś cudem już tu tragiłeś, to nie licz na to, że coś z tego skumasz i czegoś się nauczysz. Ja to mam rozumieć. To moja baza wiedzy. Inni nie muszą wiedzieć o co chodzi</p>
        </div>
      </div>
    );
  }
}
export default About;
