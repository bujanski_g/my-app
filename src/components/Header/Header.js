import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHtml5, faSass, faJs, faReact, faGit } from '@fortawesome/free-brands-svg-icons';

class Header extends Component {

  render() {
    return (

      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="/">Tu budziet logo</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/" className="nav-link">Strona domowa</Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link">O stronie</Link>
            </li>
            <li className="nav-item">
              <Link to="/Blog" className="nav-link">Blog</Link>
            </li>
            <li className="nav-item">
              <Link to="/contact" className="nav-link">Kontakt</Link>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Tutoriale
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <Link to="/Tutorials/Html5" className="dropdown-item"><FontAwesomeIcon icon={faHtml5} /> HTML5</Link>
                <Link to="/Tutorials/JavaScript" className="dropdown-item"><FontAwesomeIcon icon={faJs} /> JavaScript</Link>
                <Link to="/Tutorials/Sass" className="dropdown-item"><FontAwesomeIcon icon={faSass} /> Sass</Link>
                <Link to="/Tutorials/React" className="dropdown-item"><FontAwesomeIcon icon={faReact} /> React</Link>
              <Link to="/Tutorials/Git" className="dropdown-item"><FontAwesomeIcon icon={faGit} /> Git</Link>
              </div>
            </li>
          </ul>
        </div>
      </nav>

    );
  }
}

export default Header;
