import React, { Component } from 'react';
import './Home.sass';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHtml5, faSass, faJs, faReact, faGit } from '@fortawesome/free-brands-svg-icons';
import { Link } from 'react-router-dom';
import TodoForm from '../TodoForm/TodoForm';


class Home extends Component {

  render() {
    return (

        <div className = "container-fluid content">
          <div className = "bg">
            <div className = "Header">
              <h1> Prywatna Baza wiedzy + przy okazji poligon doświadczalny :) </h1>
              <p>Materiały pomocnicze dotyczące wszystkiego co potrzebne, do zbudowania takiej strony</p>
            </div>
            <div className = "row">
              <div className = "todo col">
                <TodoForm />
              </div>
              <div className = "goals col">
                <ul>
                  <p>Główne założenia:</p>
                  <li>Stworzyć bazę wiedzy do własnego użytku</li>
                  <li>Nabrać wprawy w pisaniu stron</li>
                  <li>Uczyć się na własnych błędach</li>
                  <li>W miarę rozwoju naprawiać błędy nooba</li>
                  <li>W pierwszym etapie stworzyć cos na zasadzie "drogi pamiętniczku"</li>
                  <li>W drugim stworzyć bloga, w pełni napisanego w React</li>
                  <li>Mieć satysfakcję z tego, że to żyje!</li>
                </ul>
              </div>
            </div>
            <div className="Links">
              <div className="row">
                <div className="col"><Link to="/Html5" className="dropdown-item"><FontAwesomeIcon icon={faHtml5} /> Html5</Link></div>
                <div className="col"><Link to="/JavaScript" className="dropdown-item"><FontAwesomeIcon icon={faJs} /> JavaScript</Link></div>
                <div className="col"><Link to="/Sass" className="dropdown-item"><FontAwesomeIcon icon={faSass} /> Sass</Link></div>
                <div className="col"><Link to="/React" className="dropdown-item"><FontAwesomeIcon icon={faReact} /> React</Link></div>
                <div className="col"><Link to="/Git" className="dropdown-item"><FontAwesomeIcon icon={faGit} /> Git</Link></div>
              </div>
            </div>
          </div>
        </div>

    );
  }
}
export default Home;
