import React, { Component } from 'react';
import './React.sass';
import renderHTML from 'react-render-html';

class RReact extends Component {
  render() {
    return (



        <div className="row content">
          <div className="menu col-2">
            <p>Spis treści</p>
          </div>
          <div className="tutorials col">
            <p className="title">FontAwesome</p>
            <p className="docs">instalacja</p>
              <ul className="code">
                <li><code>{renderHTML("$ npm i --save @fortawesome/fontawesome-svg-core")}</code></li>
                <li><code>{renderHTML("$ npm i --save @fortawesome/free-solid-svg-icons")}</code></li>
                <li><code>{renderHTML("$ npm i --save @fortawesome/react-fontawesome")}</code></li>
                <li><code>{renderHTML("$ npm i --save @fortawesome/free-brands-svg-icons")}</code></li>
                <li><code>{renderHTML("$ npm i --save @fortawesome/free-regular-svg-icons")}</code></li>
              </ul>
            <p className="docs">użycie:</p>
              <ul className="code">
                <li><code>{renderHTML("import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' - żeby używać polecenia FontAwesomeIcon")}</code></li>
                <li><code>{renderHTML("import { faCoffee } from '@fortawesome/free-solid-svg-icons' - nazwa ikonki - każdą trzeba oddzielnie")}</code></li>
                <li><code>{renderHTML("<")}FontAwesomeIcon icon='faCoffee' />  - dodanie ikonki na froncie</code></li>
              </ul>

              <p className="title">renderHTML</p>
              <p className="docs">Potrzebne, żeby wyświetlać kod html jako zwykły tekst na stronie</p>
                <p className="docs">instalacja</p>
                  <ul className="code">
                    <li><code>{renderHTML("$ npm i --save react-render-html")}</code></li>
                  </ul>
                  <p className="docs">użycie:</p>
                  <ul className="code">
                    <li><code>{renderHTML('{renderHTML("Kod który chcemy wyświetlić")}')}</code></li>
                  </ul>
          </div>
        </div>

);
}
}
export default RReact;
