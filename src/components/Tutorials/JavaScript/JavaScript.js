import React, { Component } from 'react';
import renderHTML from 'react-render-html';
import "./JavaScript.sass";

class JavaScript extends Component {
  render() {
    return (



        <div className="container content">
          <h1> Podstawy JavaScript </h1>
            <div className="js-var">
              <h1>DO POPRAWY - POUSTWIANIA W ODPOWIEDNIE MIEJSCA ITP.</h1>
              <h2>Zmienne</h2>
                <p>Deklarowanie zmiennej tekstowej (string)</p>
                  <li><code>{renderHTML("var first_name = 'Grzegorz';")}</code></li>
                <p>Dodawanie wartości tekstowej do już istniejącej zmiennej</p>
                  <li><code>{renderHTML("var last_name = first_name + ' Bujański';")}</code></li>
                <p>Deklarowanie zmiennej liczbowej</p>
                <p>Zmienna nie może zaczynać się od cyfry np: 12number, ale number12 jest ok</p>
                  <li><code>{renderHTML("var number = 12;")}</code></li>
                <p>Zmienna boolen</p>
                <p>nie można dać nazwy zmiennej true i false</p>
                  <li><code>{renderHTML("var prawda = true;")}</code></li>
                  <li><code>{renderHTML("var falsz = false;")}</code></li>
                <p>zmienna może być nulem</p>
                  <li><code>{renderHTML("var pusta = null;")}</code></li>
                <p>Zmienna może byc niezdefiniowana - nie przyjmować żadnej wartości. Są dwa sposoby zadeklarowania zmiennej bez wartości:</p>
                  <li><code>{renderHTML("var niezdefiniowana = undefined;")}</code></li>
                  <li><code>{renderHTML("var niezdefiniowana;")}</code></li>
                <p>wykonywanie obliczeń + zdefiniowanie wyniku w nowej zmiennej</p>
                  <li><code>{renderHTML("var number1 = 8;")}</code></li>
                  <li><code>{renderHTML("var number2 = 4;")}</code></li>
                  <li><code>{renderHTML("var wynik = number1 + number2;")}</code></li>
                <h2>Operatory logiczne</h2>
                <p>To nie operator logiczny - ale warte zapamiętania! Dodanie +1 do wyniku</p>
                  <li><code>{renderHTML("++")}</code></li>
                <p>j.w - warte zapamiętania! Dodaje wartość do już istniejącej zmiennej w szybszy sposób - zamiast pisać number1 = number1 + 2;</p>
                  <li><code>{renderHTML("number1 += 2;")}</code></li>
                <p>Czy mniejsze niż...</p>
                  <li><code>{renderHTML("<")}</code></li>
                <p>Czy większe niż...</p>
                  <li><code>{renderHTML(">")}</code></li>
                <p>Czy mniejsze lub równe</p>
                  <li><code>{renderHTML("<=")}</code></li>
                <p>Czy większe lub równe</p>
                  <li><code>{renderHTML(">=")}</code></li>
                <p>Czy równe (bez porównania typu zmiennej)</p>
                <p>Nie Stosujemy = bo to operator służący do przypisania wartości do zmiennej</p>
                  <li><code>{renderHTML("==")}</code></li>
                <p>Czy równe (z porównania typu zmiennej)</p>
                <p>równe - ale słóżące do porównania także typów - np. 2 == "2" da true - rozpozna że "2" to też number, ale 2 === "2" da false - bo "2" jest stringem a nie number</p>
                  <li><code>{renderHTML('===')}</code></li>
                <p>I (or)</p>
                  <li><code>{renderHTML("%%")}</code></li>
                <p>Oraz (and)</p>
                  <li><code>{renderHTML("||")}</code></li>
                <p>WAŻNE!</p>
                <p>false, 0, "" (pusty string), undefined, null, NaN - to wartości fałszywe, pozostałe zawsze będą prawdziwe.</p>
              <h2>Tablice</h2>
                <p> Tworzenie tablicy</p>
                  <li><code>{renderHTML("var table = [1, 4, 8, 15, 20, 11];")}</code></li>
                <p>Zwracanie wszystkich elementów tablicy</p>
                  <li><code>{renderHTML("for (var i = 0; i < table.length; i++) {")}</code></li>
                  <li><code>{renderHTML("console.log(table[i]);")}</code></li>
                  <li><code>{renderHTML("}")}</code></li>
              <h2>Obiekty</h2>
                <p>Tworzenie obiektu</p>
                  <li><code>{renderHTML("var person = {")}</code></li>
                  <li><code>{renderHTML('first_name: "Grzegorz",')}</code></li>
                  <li><code>{renderHTML('last_name: "Bujański"')}</code></li>
                  <li><code>{renderHTML("};")}</code></li>
                <p>Równie dobrze można najpierw stworzyć obiekt, a potem dodać wartości</p>
                  <li><code>{renderHTML("var person = {}")}</code></li>
                  <li><code>{renderHTML('person.first_name = "Grzegorz"')}</code></li>
                  <li><code>{renderHTML('person.last_name = "Bujański"')}</code></li>
                <p>Możemy tworzyć obiekty zagnieżdżone</p>
                  <li><code>{renderHTML("var person = {")}</code></li>
                  <li><code>{renderHTML('first_name: "Grzegorz",')}</code></li>
                  <li><code>{renderHTML('last_name: "Bujański"')}</code></li>
                  <li><code>{renderHTML('adress: {')}</code></li>
                  <li><code>{renderHTML("street: 'lipowa'")}</code></li>
                  <li><code>{renderHTML("city: 'Warszawa'")}</code></li>
                  <li><code>{renderHTML("}};")}</code></li>
                <p>Do obiektu można także przypisać metodę</p>
                  <li><code>{renderHTML("var person = {")}</code></li>
                  <li><code>{renderHTML('first_name: "Grzegorz",')}</code></li>
                  <li><code>{renderHTML('last_name: "Bujański"')}</code></li>
                  <li><code>{renderHTML('full_name = () {')}</code></li>
                  <li><code>{renderHTML('return this.first_name + " " + this.last_name;')}</code></li>
                  <li><code>{renderHTML('}')}</code></li>
                  <li><code>{renderHTML('}')}</code></li>
                <p>Zwracanie elementu obiektu</p>
                  <li><code>{renderHTML("person.first_name")}</code></li>
                  <li><code>{renderHTML("person.adress.city")}</code></li>
                <p>usuwanie własności z oiektu</p>
                  <li><code>{renderHTML("delete person:adress;")}</code></li>
              <h2>Funkcje</h2>
                <p>Tworzenie funkcji (Z mechanizmem sprawdzania czy podane dwa parametry)</p>
                  <li><code>{renderHTML("function przedstawSiem(imie, nazwisko) {")}</code></li>
                  <li><code>{renderHTML("if (nazwisko == undefined) {")}</code></li>
                  <li><code>{renderHTML('console.log("witam, nazywam się " + imie);')}</code></li>
                  <li><code>{renderHTML("}")}</code></li>
                  <li><code>{renderHTML("else {")}</code></li>
                  <li><code>{renderHTML('console.log("witam, nazywam się " + imie + " " + nazwisko);   // wywołujemy pisząc przedstawSie("Grzegorz", "Bujański") lub przedstawSię(first_name, last_name)')}</code></li>
                  <li><code>{renderHTML("}")}</code></li>
                  <li><code>{renderHTML("}")}</code></li>
                <p>Funckja sumująca</p>
                  <li><code>{renderHTML("function sum(number1, number2) {")}</code></li>
                  <li><code>{renderHTML("return number1 + number2;")}</code></li>
                  <li><code>{renderHTML("}")}</code></li>
                <p>Funkcja anonimowa - od razu się wywołuje - nie trzeba podawać paramertów i nazwy + możemy deklarować zmienne we wnątrz, które nie kolidują z innymi zmiennymi (o tej samej nazwie)</p>
                  <li><code>{renderHTML("(function() {")}</code></li>
                  <li><code>{renderHTML("console.log(1+2);")}</code></li>
                  <li><code>{renderHTML("})();")}</code></li>
                <p>Przypisanie wyniku funkcji do zmiennej</p>
                  <li><code>{renderHTML("var result = sum(1, 3);")}</code></li>
                <p>Wyświetlenie wyniku w logu konsoli (przydatne do pisania obsługi błędów walidacji)</p>
                  <li><code>{renderHTML("console.log(result);")}</code></li>
                <p>wywołanie funkcji przedstaw się (console.log jest już w funkcji)</p>
                  <li><code>{renderHTML("przedstawSie(first_name, last_name);")}</code></li>
                <p>document pozwala na robienie różnych rzeczy ze stroną document = "kod strony" najprosciej mówiąc. querySelector pozwala znaleźć element na stronie #container to id jakie nadaliśmy div`owi</p>
                  <li><code>{renderHTML('var container = document.querySelector("#container");')}</code></li>
                <p>Podmieni cały div mający id container (powyżej deklarowaliśmy zmienną container) na tekst "psikus"!'</p>
                  <li><code>{renderHTML('container.innerHTML = <')}h1> psikus! {renderHTML('<')}/h1> )}</code></li>
                <p>Znajdowanie pierwszego elementu na stronie (w tym przypadku {renderHTML('<')}p>)</p>
                  <li><code>{renderHTML('querySelector("p");')}</code></li>
                <p>Znajdzie wszystkich elementów (w tym przypadku {renderHTML('<')}p>)</p>
                  <li><code>{renderHTML('querySelectorAll("p");')}</code></li>
                <p>znajdzie wszystkie {renderHTML('<')}p> mające w sobie {renderHTML('<')}a></p>
                  <li><code>{renderHTML('querySelector("p a")')}</code></li>
            </div>
        </div>

);
}
}
export default JavaScript;
